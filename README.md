# SarikaNale_PB0053_Project
## Problem statement
I own a Multi-Specialty Hospital chain with locations all across the world. My hospital is famous
for Vaccination. Patients who come to my hospital (across the globe) will be given a User Card
with which they can access any of my hospital in the world.
We maintain all customers in one database. There are heaps of customers with user cards to
my hospital. So, I decided to split up the customers based on the country and load them into
corresponding country tables.
To pull the customers as per Country, my developers should know what are all the places the
Customer Data is available. So, the data extracting will be done by our Source System. They
will pull the all the relevant customer data and will give us a Data file.

# First approach using XML file and python
## Technologies used in the project
    * Python
    * MySQL

## Steps which I have followd while solving the problem
    1. Reading the problem statement and analyzing the requirements.
    2. Craeting a sample data.
        Tried to craete sample data file in CSV and XML format and after doing some trail and error will ended up to be continue with XML format.
    3. Created XML file.
    4. Craeted database and table schema in MySQL.
    5. Load the data from xml file to MySQL.
    6. In next step I have tried to connect MySQL and python using python-MySQL connectivity
    7. Download the customer list as per user's entered input.



# second approach which I tried by using HIVE 
## Hive query to create customer table
create table customers
(
h string,
name string,
cust_i int,
open_dt string,
consul_dt string,
vac_id string,
dr_name string,
state string,
country string,
dob string,
flag string
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde' with SERDEPROPERTIES ( "separatorChar" = "|")
stored as textfile;

## Hive Query to load the data from csv to hive table
load data local inpath '/home/osboxes/sarika/bigdata/data/cust.csv' into table customers;

## Hive query to craete country table which have unique country entries from customers table
CREATE TABLE t_country 
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' 
STORED AS TEXTFILE AS
WITH t AS(
SELECT DISTINCT country
FROM customers)
SELECT country
FROM t;

## Hive query to get all the customers corresponding to particular country 
select customers.name from customers left join t_country on customers.country=t_country.country where customers.country='IND';